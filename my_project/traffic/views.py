from django.shortcuts import render,get_object_or_404,redirect
from .models import Issue_Challan,Vehicle,Owner, PolicePersonal, Rules,Payment
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, permission_required
from .forms import ChallanForm,VehicleForm
from django.db import transaction
from django.http import HttpResponseRedirect,HttpResponse
@login_required(login_url = 'login')    



def issue_challan(request):
    return render(request,'issuechallanpage.html')


def challan_list(request):
    return render(request, 'challan.html')


def login_(request):
    if request.method == 'GET':
        return render(request, 'authenticate.html')
    if request.method == 'POST':
        username_ = request.POST.get('username', '')
        entered_password = request.POST.get('password', '')
        user = authenticate(username=username_, password=entered_password)
        print(user)
        if user is not None:
            login(request, user)
        return redirect('issue_challan')

def logout_(request):
    logout(request)
    return redirect('login')

@permission_required('traffic.challan_form', login_url='login')
def challan_form(request):
    submitted = False
    if request.method=='POST':
        form = ChallanForm(request.POST,request.FILES)    
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('challan_form?submitted=True')
    else:
        form = ChallanForm
        if 'submitted' in request.GET:
            submitted = True
    return render(request,'issuechallan.html',{'form':form,'submitted':submitted})

@permission_required('traffic.challan_form', login_url='login')
def vehicle_form(request):
    submitted = False
    if request.method=='POST':
        form = VehicleForm(request.POST)    
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('vehicle_form?submitted=True')
    else:
        form = VehicleForm
        if 'submitted' in request.GET:
            submitted = True
    return render(request,'vehicleupdate.html',{'form':form,'submitted':submitted})

def e_challan(request):
    return render(request,'aboutechallan.html')

@permission_required('traffic.vehicle_details', login_url='login')    
def vehicle_details(request):
    if request.method == 'POST':
        value = request.POST.get('value')
        try:
            record = Vehicle.objects.get(Vehicle_Number=value)
            return render(request, 'vehicledetails.html', {'record': record})
        except Vehicle.DoesNotExist:
            return render(request, 'nodetails.html')
    else:
        return render(request, 'vehiclefetch.html')

@permission_required('traffic.challanhistory', login_url='login')  
def challanhistory(request):
    challan=Issue_Challan.objects.all()
    return render(request,'challanhistory.html',{'challan':challan})

def view_challan(request):
    if request.method == 'POST':
        value = request.POST.get('value')
        try:
            vehicle = Vehicle.objects.get(Vehicle_Number=value)
            record = Issue_Challan.objects.filter(vehicle=vehicle)
            if record:
                return render(request, 'viewchallan.html', {'record': record})
            else:
                return render(request, 'noresults.html')
        except Vehicle.DoesNotExist:
            return render(request, 'noresults.html')
    else:
        return render(request, 'challan_list.html')


def e_contact(request):
    return render(request,'econtact.html')

def registeredvehicles(request):
    vehi = Vehicle.objects.all()
    return render(request,'regi.html',{'vehi':vehi})


def get_challan(request):
    if request.method == 'POST':
        value = request.POST.get('value')
        
    return render(request, 'challan_list.html')

def pay_challan(request):
    if request.method == 'POST':
        value = request.POST.get('value')
        vehicle = Vehicle.objects.get(Vehicle_Number=value)
        record = Issue_Challan.objects.filter(vehicle=vehicle)
        if record:
            if request.POST.get('submit') == 'Pay Challan':
                checkboxes = request.POST.getlist('challan_select')
                total_amount = 0
                paid_records = []
                for checkbox in checkboxes:
                    challan = Issue_Challan.objects.get(id=checkbox)
                    total_amount += challan.Fine_Amount
                    paid_records.append(Payment(vehicle=vehicle, challan=challan))
                
                Payment.objects.create(paid_records)
                Issue_Challan.objects.filter(id__in=checkboxes).delete()
                return render(request, 'confirm_payment.html', {'total_amount': total_amount})
            else:
                return render(request, 'paychallan.html', {'record': record})
        else:
            return render(request, 'noresults.html')
    else:
        return render(request, 'challan_list.html')

def challan_payment(request):
    if request.method == 'POST':
        amount = request.POST.get('value')
        vehicle_number = request.POST.get('vehicle_number')
        for i in request.POST:
            for j in Issue_Challan.objects.all():
                if i == 'challan-' + str(j.id):
                    if request.POST.get(i):
                        j.delete()
                        payment = Payment(vehicle_number=vehicle_number, amount_paid=amount)
                        payment.save()
                        message = 'Challan was Cleared'
                        return render(request, 'confirm_payment.html', {'message':message})


def paymnt(request):
    if request.method == 'POST':
        value = request.POST.get('value')
        try:
            record = Payment.objects.filter(vehicle_number=value)
            if record:
                return render(request, 'payhistory.html', {'record': record})
            else:
                return render(request, 'no.html')
        except Vehicle.DoesNotExist:
            return render(request, 'no.html')
    else:
        return render(request, 'paylist.html')
