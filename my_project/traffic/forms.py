from django import forms
from django.forms import ModelForm
from .models import Issue_Challan,Vehicle


class ChallanForm(ModelForm):
    class Meta:
        model = Issue_Challan
        fields = "__all__"
    

class VehicleForm(ModelForm): 
    class Meta:
        model = Vehicle
        fields = "__all__"
        